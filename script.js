$(window).on("load", function () {
  // makes sure the whole site is loaded
  setTimeout(function () {
    $(".pageloader").slideUp("slow");
  }, 2000);
  setTimeout(function () {
    $(".logo_loader").fadeOut("slow");
  }, 1500);
  $("body").delay(2000).css({ overflowY: "scroll" });
});

var signaturePad = new SignaturePad(document.getElementById("signature-pad"), {
  backgroundColor: "rgba(255, 255, 255, 0)",
  penColor: "rgb(0, 0, 0)",
  dotSize: 1,
});
var saveButton = $("#save");
var cancelButton = $("#clear");
var path_img = $(".get_signature");
saveButton.click(function (event) {
  var data = signaturePad.toDataURL("image/jpg");
  $(".get_signature").text(data);
});
cancelButton.click(function (event) {
  signaturePad.clear();
  $(".get_signatureimg").attr("src", "");
  $("#signature_val").val("");
  if ($("#signature_val").hasClass("error")) {
    $("#signature_val").next().show();
  }
});
$(".form_signature").submit(function (e) {
  //NOTE: Removed "#"
  e.preventDefault();
  if ($(".did-floating-input").valid()) {
    if (!signaturePad.isEmpty()) {
      $(".thankyou_step").fadeIn();
      $(".first_step_package").hide();
      $(".second_step_package").hide();
      setTimeout(function () {
        window.location.reload();
      }, 5000);
    }
  }
});
$(".btn_send").submit(function (e) {
  if (!signaturePad.isEmpty()) {
    $(".get_signatureimg").attr("src", signaturePad.toDataURL());
    setTimeout(function () {
      $(".form_signature").reset();
      window.location.reload();
    }, 5000);
  }
});

$("#signature_val").keydown(function (e) {
  e.preventDefault();
});
signaturePad.onEnd = function () {
  $("#signature_val").val(signaturePad.toDataURL());
  console.log($("#signature_val").val(), "image base 64");

  if (
    $("#signature_val").val().length > 0 &&
    $("#signature_val").hasClass("error")
  ) {
    $("#signature_val").next().hide();
  }
};
signaturePad.onBegin = function () {
  if (
    $("#signature_val").val().length === 0 &&
    $("#signature_val").hasClass("error")
  ) {
    $("#signature_val").next().hide();
  }
};

$(".link_tabs").on("click", function () {
  var numberIndex = $(this).index();
  if (!$(this).is("active")) {
    $(".link_tabs").removeClass("active");
    $(".content_tabs .item_tabs").removeClass("active");

    $(this).addClass("active");
    $(".content_tabs")
      .find(".item_tabs:eq(" + numberIndex + ")")
      .addClass("active");
  }
});

$(".radios_client_info .select_host").on("click", function () {
  $(".radios_client_info .select_host").parent("label").removeClass("checked");
  $(this).parent("label").addClass("checked");
});

$(".radios_client_info .select_host:checked")
  .parent("label")
  .addClass("checked");

$(".item__host").on("click", function (e) {
  $(this).each(function (i) {
    var data = $("<div class='special'>").append($(this).contents().clone());
    $(".selected_host").html(data);
  });

  $(".first_step_package").fadeOut(function () {
    $(".second_step_package").fadeIn();
  });
});
$(".back_to_first_package").on("click", function () {
  $(".second_step_package").fadeOut(function () {
    $(".selected_host").find(".basic-plan.checked").remove();
    $(".first_step_package").fadeIn();
  });
});

$("#save").on("click", function (e) {
  e.preventDefault();
});

$("#clear").on("click", function (e) {
  e.preventDefault();
});
$(document).ready(function () {
  $(".form_signature").validate();
});
